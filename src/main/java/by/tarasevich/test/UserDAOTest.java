package by.tarasevich.test;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import by.tarasevich.library.dao.DAOException;
import by.tarasevich.library.dao.impl.UserDAO;
import by.tarasevich.library.entity.User;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;

/**
 *
 * @author рома
 */
public class UserDAOTest {
    
    @Test
    public void testNumberOfRegisteredUsers() throws DAOException{

        Integer size = UserDAO.getInstance().showAllUsers().size();
        Integer actual = 9;
        assertEquals(actual,size);
    }

 

}
