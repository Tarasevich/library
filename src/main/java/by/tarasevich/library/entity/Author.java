package by.tarasevich.library.entity;

import java.io.Serializable;
import java.util.Objects;

/** 
  *Class Author is an entity-class. 
  *The object of this class is the author of the object of class Book
 */
public class Author implements Serializable {

    /**
     * @param id  is the unique object identifier
     * @param name is the name of the author
     */
    private int id;
    private String name;
  

    public Author() {
    }

    public Author(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * getters and setters of the class fields
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + this.id;
        hash = 47 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Author other = (Author) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.name;
    }
}

