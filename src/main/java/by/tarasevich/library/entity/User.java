package by.tarasevich.library.entity;


import by.tarasevich.library.dao.DAOException;
import by.tarasevich.library.dao.impl.UserDAO;
import by.tarasevich.library.util.Util;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.primefaces.event.RowEditEvent;

@ManagedBean(name = "user")
@SessionScoped //viewScoped
public class User implements Serializable {

    /**
     * @param id            the unique identifier 
     * @param ticketNumber  number of user's ticket 
     * @param name          name of the user 
     * @param password      user's password
     * @param role          user's role: admin or reader
     */
    private static final long serialVersionUID = 1L;
    private int id;
    private int ticketNumber;
    private String name;
    private String password;
    private String role;
  
     
    public User() {
    }

    public User(int id, int ticketNumber, String name, String password,  String role) {
        this.id = id;
        this.ticketNumber = ticketNumber;
        this.name = name;
        this.password = password;
        this.role = role;
    }
    
    /**
     *  list of all users registered in the library
     */
      private static final ArrayList<User> userList = new ArrayList<>();
      
      /**
       * logger for this class
       */
      private static Logger logger = Logger.getLogger(User.class); 
      
      /**
       * method is called to get a list of all user registered in the library
       * @return users list 
       */
       public ArrayList<User> getUserList() {
        return userList;
    }
       
       /**
        * when the  page (pagesLibr/user.xhtml) is opened by user , this method is called in first order, at the beggining this method deletes all entries from the user list,
        * then gets the current list of users from database and shows it to the user in the form of a table on the jsf page
        * if method cant do this, he's catching DAOException
        */
        @PostConstruct
        public void init(){
        try {
            userList.clear();
            userList.addAll(UserDAO.getInstance().getUserList());
        } catch (DAOException ex) {
          logger.log(Level.ERROR,"Can not show all users");
        }
    } 
       
    /**
     * (pagesLibr/user.xhtml) when the user fills in the form of adding a new user and click button "Add user" this method is added
     *   user to the current users list and user can see on jsf page that new user  has been added, then method adds user to the database 
     *   and updates the form fields for adding a new user. if method cant do this, he's throwing DAOException
     * @return null
     * @throws DAOException 
     */
    public String addAction() throws DAOException {
        User user = new User(this.id,this.ticketNumber, this.name,this.password,this.role);
        userList.add(user);
        UserDAO.getInstance().addUser(user);
        logger.log(Level.INFO,"User + " + this.name + " has been added to the user list");
        id = 0;
        ticketNumber = 0;
        name ="";
        password="";
        role="";
      return null;
    }
    /**
     * (pagesLibr/user.xhtml) when user clicks on the button with pencil-image, the form of the table is updated and two buttons are opened: delete user and exit the edit
     * @param event
     * @throws DAOException
     * @throws SQLException 
     */
     public void onEdit(RowEditEvent event) throws DAOException, SQLException { 
         
        FacesMessage msg = new FacesMessage("User Edited",((User) event.getObject()).getName());  
        FacesContext.getCurrentInstance().addMessage(null, msg);      
    }  
     
       /**
        * (pagesLibr/user.xhtml) when user clicks the button "delete", user can see message in the right corner of the screen that his current user was deleted.
       * then user can see that his current user after deletion was deleted from the table, and then method deletes current user from the database.
       * if method cant do this, he's throwing DAOException
        * @param event
        * @throws DAOException 
        */
    public void onCancel(RowEditEvent event) throws DAOException {  
        FacesMessage msg = new FacesMessage("Читатель удален");   
        FacesContext.getCurrentInstance().addMessage(null, msg); 
        userList.remove((User) event.getObject());     
        UserDAO.getInstance().deleteUser((User) event.getObject());
        logger.log(Level.INFO,"User + " + this.name + " has been deleted from the user list");
      
    }  
    /**
     * method which checked is users list contains user with this name and this password, inputing in log in form 
     * if method cant do this, he's throwing SQLException
     * 
     * @return
     * @throws SQLException 
     */
    public String loginProject() throws SQLException{
       boolean result = UserDAO.getInstance().login(name, password,role);
       if(result && (role.equalsIgnoreCase("admin"))){
           HttpSession session = Util.getSession();
           session.setAttribute("user_name", name);
           logger.log(Level.INFO,"Admin "+this.getName()+" sign in on the website");
          return "books";
       }else if(result  && (role.equalsIgnoreCase("reader"))){
           HttpSession session = Util.getSession();
           session.setAttribute("user_name", name);
           logger.log(Level.INFO,"Reader "+this.getName()+" sign in on the website");
           return "books";   
       }else if(result  && (role.equalsIgnoreCase("librarian"))){
          HttpSession session = Util.getSession();
           session.setAttribute("user_name", name);
           logger.log(Level.INFO,"Librarian "+this.getName()+" sign in on the website");
           return "books";
       }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "Invalid Login!",
                    "Please Try Again!"));
            return "error";
        }
    }
    /**
     * log out to the start-page
     * @return 
     */
    public String logout(){
        HttpSession session = Util.getSession();
        session.invalidate();
        logger.log(Level.INFO,this.getName()+" sign out from the website");
        return "exit";
    }
    
    /**
     * navigate to the main page
     * @return 
     */
    public String toMainPage(){
        return "/pages/books.xhtml";
    }
    
    /**
     * navigate to the user page
     * @return 
     */
    public String toUsersPage(){
        return "/pagesLibr/user.xhtml";
    }
    
    /**
     * navigate to the notes page
     * @return 
     */
    public String toNotesPage(){
        return "/pagesLibr/notes.xhtml";
    }
    
    /**
     * navigate to the book page
     * @return 
     */
    public String toBookPage(){
        return "/pagesLibr/book.xhtml";
    }
    
    /**
     * navigate to the libr page 
     * @return 
     */
    public String toLibrPage(){
        return "/pagesLibr/librarian.xhtml";
    }
     
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + this.id;
        hash = 59 * hash + this.ticketNumber;
        hash = 59 * hash + Objects.hashCode(this.name);
        hash = 59 * hash + Objects.hashCode(this.password);
        hash = 59 * hash + Objects.hashCode(this.role);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.ticketNumber != other.ticketNumber) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.role, other.role)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.name;
    }

    
 
}
