package by.tarasevich.library.entity;

import by.tarasevich.library.dao.DAOException;
import by.tarasevich.library.dao.impl.IssuingBookDAO;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.primefaces.event.RowEditEvent;

/**
 * class IssuingBook shows which books were given to which readers
 * @author рома
 */
@ManagedBean(name = "note")
@ViewScoped
public class IssuingBook implements Serializable{
    
    /**
     * @param id             unique identifier of object
     * @param issuingDate    date when book was given to the user
     * @param returnDate     date when book was given back to the library
     * @param user           user which took book
     * @param book           which book was given to the user
     * @param location       shows to where book was given: in the Hall or on the House
     */
    private int id;
    private String issuingDate;
    private String returnDate;
    private String user;
    private String book;
    private String location;

    public IssuingBook() {
    }

    public IssuingBook(int id, String user, String book, String issuingDate, String location) {
        this.id = id;
        this.user = user;
        this.book = book;
        this.issuingDate = issuingDate;
        this.location = location;
    }

    public IssuingBook(int id, String user, String book, String issuingDate, String returnDate, String location) {
       this.id = id;
        this.user = user;
        this.book = book;
        this.issuingDate = issuingDate;
        this.returnDate = returnDate;
        this.location = location;
    }
    
    /**
     * list which contains current notes about issuing books 
     */
    private static final ArrayList<IssuingBook> noteList = new ArrayList<>();
    /**
     * logger for this class
     */
    private static Logger logger = Logger.getLogger(IssuingBook.class);
      
    /**
     * method is called to get list of current notes
     * @return list 
     */
       public ArrayList<IssuingBook> getNoteList() {
        return noteList;
    }
       /**
        * when the  page (pagesLibr/notes.xhtml) is opened by user , this method is called in first order, at the beggining this method deletes all entries from the note list,
        * then gets the current list of notes from database and shows it to the user in the form of a table on the jsf page, if method can not show notes he's throwing DAOException
        */
     @PostConstruct
    public void init(){
        try {
            noteList.clear();
            noteList.addAll(IssuingBookDAO.getInstance().showNotes());
        } catch (DAOException ex) {
            logger.log(Level.ERROR,"Can not show all notes");
        }
    }
    /**
     * pagesLibr/notes.xhtml) when the user fills in the form of adding a new note and click button "Add note" this method is added
     * note to the current note list and user can see on jsf page that note has been added, then method adds note to the database in the table "issuing books"
     * and updates the form fields for adding a new note, if method cant do this, he's throwing DAOException
     * @return null
     * @throws DAOException 
     */
    
     public String addAction() throws DAOException {
    
        IssuingBook note = new IssuingBook(this.id,this.user,this.book,this.issuingDate,this.returnDate,this.location);
        noteList.add(note);
        IssuingBookDAO.getInstance().addNote(note);
        logger.log(Level.INFO,"Note + " + this.id + " has been added to the note list");
        id = 0;
        user = "";
        book = "";
        issuingDate = null;
        returnDate = null;
        location = "";
      return null;
    }
     /**
      * pagesLibr/notes.xhtml) when user clicks on the button with pencil-image, the form of the table is updated and two buttons are opened: delete note and exit from edit
      * @param event
      * @throws DAOException
      * @throws SQLException 
      */
      public void onEdit(RowEditEvent event) throws DAOException, SQLException { 
         
        FacesMessage msg = new FacesMessage("Запись обновлена",((IssuingBook) event.getObject()).getUser());  
        FacesContext.getCurrentInstance().addMessage(null, msg);   
    }
      
      /**
       * (pagesLibr/notes.xhtml) when user clicks the button "delete", user can see message in the right corner of the screen that his current note was deleted.
       * then user can see that his current note after deletion was deletion from the table, and then method deletes current note from the database.
       * if method cant do this, he's throwing DAOException
       * @param event
       * @throws DAOException 
       */
       public void onCancel(RowEditEvent event) throws DAOException {  
        FacesMessage msg = new FacesMessage("Запись удалена");   
        FacesContext.getCurrentInstance().addMessage(null, msg); 
        noteList.remove((IssuingBook) event.getObject());   
      IssuingBookDAO.getInstance().deleteNote((IssuingBook) event.getObject());
      logger.log(Level.INFO,"Note + " + this.id + " has been deleted from the note list"); 
    }  
       
       public Integer howManyNotes() throws DAOException{
           return IssuingBookDAO.getInstance().showNotes().size()+1;
       }
       
    /**
     * method is called when it necessary to get all books which is stored in library at this moment,if method cant do this, he's throwing DAOException
     * @return books list 
     * @throws DAOException 
     */
       public ArrayList<Book> getBooks() throws DAOException{
         return IssuingBookDAO.getInstance().getBooks();
     }
      /**
       * method is called when it necessary to get all users which is registered in the library,if method cant do this, he's throwing DAOException
       * @return users list
       * @throws DAOException 
       */ 
      public ArrayList<User> getReaders() throws DAOException{
        return IssuingBookDAO.getInstance().getUsers();
     }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIssuingDate() {
        return issuingDate;
    }

    public void setIssuingDate(String issuingDate) {
        this.issuingDate = issuingDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

 
    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IssuingBook that = (IssuingBook) o;

        if (id != that.id) return false;
        if (issuingDate != null ? !issuingDate.equals(that.issuingDate) : that.issuingDate != null) return false;
        if (returnDate != null ? !returnDate.equals(that.returnDate) : that.returnDate != null) return false;
        if (user != null ? !user.equals(that.user) : that.user != null) return false;
        if (book != null ? !book.equals(that.book) : that.book != null) return false;
        return location != null ? location.equals(that.location) : that.location == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (issuingDate != null ? issuingDate.hashCode() : 0);
        result = 31 * result + (returnDate != null ? returnDate.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (book != null ? book.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "IssuingBook{" +
                "id=" + id +
                ", issuingDate=" + issuingDate +
                ", returnDate=" + returnDate +
                ", user=" + user + 
                ", book=" + book +
                ", location='" + location + '\'' +
                '}';
    }
}

