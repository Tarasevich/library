package by.tarasevich.library.entity;

import by.tarasevich.library.dao.DAOException;
import by.tarasevich.library.dao.impl.BookDAO;
import by.tarasevich.library.dao.impl.GenreDAO;
import by.tarasevich.library.pool.impl.ConnectionPool;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.primefaces.event.RowEditEvent;
import org.primefaces.model.UploadedFile;


/**
 * Creates and identifies objects of this class. 
 *
 */
@ManagedBean(name = "book")
@ViewScoped
public class Book implements Serializable {
/**
 * @param id             the unique identifier 
 * @param author         the author of this book
 * @param name           the title of the book
 * @param genre          each book belongs to a particular genre
 * @param content        pdf file which is stored in database
 * @param pageCount      number of pages per book
 * @param image          jpeg file which is stored in database
 * @param exemplarCount  number of books which is stored in library
 */
    private int id;
    private String author;
    private String name;
    private String genre;
    private byte[] content;
    private int pageCount;
    private byte[] image;
    private int exemplarCount;
    
    private  UploadedFile fileImage;
    private  UploadedFile filePDF;


    public Book() {
    }
      public Book(int id, String author, String name, String genre, int pageCount, int exemplarCount) {
        this.id = id;
        this.author = author;
        this.name = name;
        this.genre = genre;
        this.pageCount = pageCount;
        this.exemplarCount = exemplarCount;
    }
      
        public Book(int id, String author, String name, String genre,  byte[] content, int pageCount, UploadedFile fileImage  , int exemplarCount) {
        this.id = id;
        this.author = author;
        this.name = name;
        this.genre = genre;
        this.content = content;
        this.pageCount = pageCount;
        this.fileImage = fileImage;
        this.exemplarCount = exemplarCount;
    }
    
   

    public Book(int id, String author, String name, String genre, byte[] content, int pageCount, byte[] image, int exemplarCount) {
        this.id = id;
        this.author = author;
        this.name = name;
        this.genre = genre;
        this.content = content;
        this.pageCount = pageCount;
        this.image = image;
        this.exemplarCount = exemplarCount;
    }
    
    
    /**
     *  list of all books available in the library
     */
     private static final ArrayList<Book> bookList = new ArrayList<>();
     
     /**
      * getting logger for this class
      */
     private static Logger logger = Logger.getLogger(Book.class);
 
     /**
      * method is called to get a list of all books available in the library
      * @return  list of all books available in the library
      */
       public ArrayList<Book> getBookList() {
        return bookList;
    }
       
       /**
        * when the  page (pagesLibr/book.xhtml) is opened by user , this method is called in first order, at the beggining this method deletes all entries from the  book list,
        * then gets the current list of books from database and shows it to the user in the form of a table on the jsf page, if method can not show books he's catching DAOException
        */
     @PostConstruct
    public void init(){
        try {
            bookList.clear();
            bookList.addAll(BookDAO.getInstance().showBooks());
        } catch (DAOException ex) {
           logger.log(Level.ERROR,"Can not show all books!");
        }
    }
    
    
    
    
    /**
     *  (pagesLibr/book.xhtml) when the user fills in the form of adding a new book and click button "Add book" this method is added
     *   book to the current books list and user can see on jsf page that book has been added, then method adds book to the database 
     *   and updates the form fields for adding a new book, if method can not add  new book he throws DAOException
     * @return null
     * @throws DAOException 
     */
     public String addAction() throws DAOException {
        Book book = new Book(this.id,this.author,this.name,this.genre,this.content,this.pageCount,this.fileImage,this.exemplarCount);
        bookList.add(book);
        BookDAO.getInstance().addBook(book);
        logger.log(Level.INFO,"Book + " + this.name + "has been added to the book list");
        id = 0;
        author = "";
        name ="";
        genre=""; 
        pageCount=0;
       
        exemplarCount = 0;
        
      return null;
    }
  
           
     /**
      * (pagesLibr/book.xhtml) when user clicks on the button with pencil-image, the form of the table is updated and two buttons are opened: delete book and exit the edit
      * @param event
      * @throws DAOException
      * @throws SQLException 
      */
      public void onEdit(RowEditEvent event) throws DAOException, SQLException { 
         
        FacesMessage msg = new FacesMessage("Book Edited",((Book) event.getObject()).getName());  
        FacesContext.getCurrentInstance().addMessage(null, msg);   
    }
      
      /**
       * (pagesLibr/book.xhtml) when user clicks the button "delete", user can see message in the right corner of the screen that his current book was deleted.
       * then user can see that his current book after deletion was deleted from the table, and then method deletes current book from the database,
       * if method can not delete book he throws DAOException
       * @param event
       * @throws DAOException 
       */
       public void onCancel(RowEditEvent event) throws DAOException {  
        FacesMessage msg = new FacesMessage("Book Cancelled");   
        FacesContext.getCurrentInstance().addMessage(null, msg); 
        bookList.remove((Book) event.getObject());
       BookDAO.getInstance().deleteBook((Book) event.getObject());
       logger.log(Level.INFO,"Book + " + this.name + " has been deleted from book list");      
    }  
       /**
        * method is called when it is necessary to see list of authors stored in database
        * @return list of all authors from database
        * @throws DAOException 
        */
     public ArrayList<Author> getAuthors() throws DAOException{
         return BookDAO.getInstance().getAuthors();
     }
     /**
      *  method is called when it is necessary to see list of genres stored in database
      * @return list of all genres from database
      * @throws DAOException 
      */
     public ArrayList<Genre> getGenres() throws DAOException{
         return GenreDAO.getInstance().getGenreList();
     }
     
       public Integer howManyBooksInLibr() throws DAOException{
        return BookDAO.getInstance().showBooks().size()+3;
    }
     /**
      * method is called to get pdf file of current book with the help of statemt to database, if he can't do this, he's catching SQLException
      */
        public void fillPdfContent() {
        ResultSet resultset = null;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try(Statement statement = connection.createStatement()){
           resultset = statement.executeQuery("select content from book where book_id="+this.getId());
           while(resultset.next()){
               this.setContent(resultset.getBytes("content"));
           }
        } catch (SQLException ex) {
          logger.log(Level.ERROR,"Can not find pdf content!");
        }finally{
            if(connection != null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }  
    }

     
     
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public int getExemplarCount() {
        return exemplarCount;
    }

    public void setExemplarCount(int exemplarCount) {
        this.exemplarCount = exemplarCount;
    }

    public UploadedFile getFileImage() {
        return fileImage;
    }

    public void setFileImage(UploadedFile fileImage) {
        this.fileImage = fileImage;
    }

    public  UploadedFile getFilePDF() {
        return filePDF;
    }

    public void setFilePDF( UploadedFile filePDF) {
        this.filePDF = filePDF;
    }
 
 
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (id != book.id) return false;
        if (pageCount != book.pageCount) return false;
        if (exemplarCount != book.exemplarCount) return false;
        if (author != null ? !author.equals(book.author) : book.author != null) return false;
        if (name != null ? !name.equals(book.name) : book.name != null) return false;
        if (genre != null ? !genre.equals(book.genre) : book.genre != null) return false;
        if (!Arrays.equals(content, book.content)) return false;
        return Arrays.equals(image, book.image);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (genre != null ? genre.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(content);
        result = 31 * result + pageCount;
        result = 31 * result + Arrays.hashCode(image);
        result = 31 * result + exemplarCount;
        return result;
    }

    @Override
    public String toString() {
        return this.name;
                
   
    }
}
