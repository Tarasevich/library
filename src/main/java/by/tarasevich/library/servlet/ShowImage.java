package by.tarasevich.library.servlet;

import by.tarasevich.library.controllers.SearchController;


import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;


@WebServlet(name = "ShowImage",
urlPatterns = {"/ShowImage"})
public class ShowImage extends HttpServlet {

    private static Logger logger = Logger.getLogger(ShowImage.class);
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            response.setContentType("image/jpeg");
        OutputStream outputStream = response.getOutputStream();
        try{
            int id = Integer.valueOf(request.getParameter("id"));
            SearchController searchController = (SearchController)request.getSession(false).getAttribute("searchController");
            byte[] image = searchController.getImage(id); 
            response.setContentLength(image.length);
            outputStream.write(image);
        }catch(Exception e){
           logger.log(Level.ERROR,"can not show image content");
        }finally {
            outputStream.close();
        }
    }
     @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request,response);
    }

    @Override
    protected  void doPost(HttpServletRequest request ,HttpServletResponse response)
        throws ServletException, IOException{
        processRequest(request,response);
    }
    @Override
    public String getServletInfo(){
        return "Short description";
    }
}

