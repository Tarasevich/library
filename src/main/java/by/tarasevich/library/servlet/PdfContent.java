/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.tarasevich.library.servlet;

import by.tarasevich.library.controllers.SearchController;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author рома
 */

@WebServlet(name = "PdfContent",
urlPatterns = {"/PdfContent"})
public class PdfContent extends HttpServlet {
    
    private static Logger logger = Logger.getLogger(PdfContent.class);
    
     protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/pdf");
        OutputStream out = response.getOutputStream();
        try {
            int id = Integer.valueOf(request.getParameter("id"));
            SearchController searchController = (SearchController) request.getSession(false).getAttribute("searchController");
            byte[] content = searchController.getContent(id);
            response.setContentLength(content.length);
            out.write(content);
        } catch (Exception ex) {
          logger.log(Level.ERROR,"PDF content can not be loaded");
        } finally {
            out.close();
        }
    }
     
      @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
      @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
     @Override
    public String getServletInfo() {
        return "Short description";
    }
    
    
    
    
    
}
