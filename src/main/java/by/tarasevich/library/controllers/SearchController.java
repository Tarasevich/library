/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.tarasevich.library.controllers;

import by.tarasevich.library.dao.DAOException;
import by.tarasevich.library.entity.Book;
import by.tarasevich.library.enumType.SearchType;
import by.tarasevich.library.pool.impl.ConnectionPool;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author рома
 */
@ManagedBean(eager = true)
@SessionScoped
public class SearchController implements Serializable  {


    /**
     * @param requestFromPage
     * @param booksOnPage - books count which user can see ot page, default value = 2
     * @param selectedGenreId - genre name which was clicked by user on page, contains genre id
     * @param selectedLetter - one letter in search string which was clicked by user
     * @param selectedPageNumber - when user navigate from page to page  this field contains current number of page, default value = 1
     * @param totalBooksCount - all books which was found by user : by genre search or by letter search
     * @param pageNumbers - number of page which show the results of searching for books
     * @param searchType - contains current type of search: title or author
     * @param searchString - contains current search string
     * @param searchList - contains all types of searching
     * @param currentBookList - current list of books for showing on page
     * @param currentSql - last sql inquiry without limit
     */

    private boolean requestFromPager;
    private int booksOnPage = 2;
    private int selectedGenreId; // выбранный жанр
    private char selectedLetter; // выбранная буква алфавита
    private long selectedPageNumber = 1; // выбранный номер страницы в постраничной навигации
    private long totalBooksCount; // общее кол-во книг (не на текущей странице, а всего), нужно для постраничности
    private ArrayList<Integer> pageNumbers = new ArrayList<>(); // общее кол-во страниц, на которых отображается результаты поиска книг
    private SearchType searchType;// хранит выбранный тип поиска
    private String searchString; // хранит поисковую строку
    private static Map<String, SearchType> searchList = new HashMap<String, SearchType>(); // хранит все виды поисков (по автору, по названию)
    private ArrayList<Book> currentBookList; // текущий список книг для отображения
    private String currentSql;// последний выполнный sql без добавления limit
    private static Logger logger = Logger.getLogger(SearchController.class);

    /**
     * @param SQL_SHOW_ALL_BOOKS - sql inquiry to get all books from database
     */
    public static final String SQL_SHOW_ALL_BOOKS = "SELECT book_id, book_name, exemplar_count, page_count, image, genre_name,author_name FROM book\n" +
                                                    "INNER JOIN author a  on book.author_id=a.author_id\n" +
                                                    "INNER JOIN genre g  on book.genre_id=g.genre_id";

    /**
     * constructor which is shows all books from database, than initialize bundle of EN/RU support, at the end put at searchList two types of search: by author and by book name
     */
    public SearchController()  {
        showBooks();
        ResourceBundle bundle = ResourceBundle.getBundle("by.tarasevich.library.nls.messages", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        searchList.put(bundle.getString("author_name"), SearchType.AUTHOR);
        searchList.put(bundle.getString("book_name"), SearchType.TITLE);
       
    }
     
    /**
     *  method is called to get all books from database, after sql inquiry @param totalBooksCount contains number of books which was getting from database
     *  than calling method fillPageNumbers, which by the number of books found determines how many pages are needed to display books in page mode
     * @param sql inquiry
     */
       public void getBooks(String sql){
             StringBuilder sqlBuilder = new StringBuilder(sql);
             currentSql = sql;             
             ResultSet resultSet = null;
             Connection connection = ConnectionPool.getInstance().takeConnection();
             try(Statement statement = connection.createStatement()){   
              if (!requestFromPager) {
                resultSet = statement.executeQuery(sqlBuilder.toString());
                resultSet.last();
                totalBooksCount = resultSet.getRow();
                fillPageNumbers(totalBooksCount, booksOnPage);
            }
               if (totalBooksCount > booksOnPage) {
                sqlBuilder.append(" limit ").append(selectedPageNumber * booksOnPage - booksOnPage).append(",").append(booksOnPage);
            }
               
              resultSet = statement.executeQuery(sqlBuilder.toString());
              
              currentBookList = new ArrayList<>();
              
              while(resultSet.next()){
                   Book book = new Book();
                book.setId(resultSet.getInt("book_id"));
                book.setName(resultSet.getString("book_name"));
                book.setGenre(resultSet.getString("genre_name"));
                book.setAuthor(resultSet.getString("author_name"));
                book.setExemplarCount(resultSet.getInt("exemplar_count"));
                book.setPageCount(resultSet.getInt("page_count"));
               // book.setImage(resultSet.getBytes("image"));
               // book.setContent(resultSet.getBytes("content"));
               currentBookList.add(book);
        }
         }catch (SQLException e) {
                 logger.log(Level.ERROR,"Can not find book by search");
        }finally {
            if(connection != null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
              }


    public void showBooks()  {
            getBooks(SQL_SHOW_ALL_BOOKS);
    }
        
         private void submitValues(Character selectedLetter, long selectedPageNumber, int selectedGenreId, boolean requestFromPager) {
        this.selectedLetter = selectedLetter;
        this.selectedPageNumber = selectedPageNumber;
        this.selectedGenreId = selectedGenreId;
        this.requestFromPager = requestFromPager;

    }

    /**
     * method is called to get books from database with sql inquiry searching by genre id
     * @return updated jsf page with results of sql inquiry
     */
    public String findBooksByGenre()  {
        
          Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
         submitValues(' ', 1, Integer.valueOf(params.get("genre_id")), false);

         getBooks("SELECT book_id,book_name,page_count, exemplar_count, author_name, genre_name, image FROM book\n" +
                "INNER JOIN author a ON book.author_id = a.author_id\n" +
                        "INNER JOIN genre g ON book.genre_id = g.genre_id\n" +
                        "WHERE g.genre_id =" + selectedGenreId + "  ORDER BY  book_name");
         
          return "books";
    }

    /**
     *  method is called to get books from database with sql inquiry searching by first letter
     * @return list of found books
     * @return updated jsf page with results of sql inquiry
     */
        public String findBooksByLetter() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
         selectedLetter = params.get("letter").charAt(0);
        
        getBooks("SELECT book_id, book_name,exemplar_count, page_count,author_name,genre_name,image FROM book\n"+
                "INNER JOIN author a ON book.author_id = a.author_id\n" +
                "INNER JOIN genre g ON book.genre_id = g.genre_id\n"+
                "WHERE substr(book_name,1,1) = '"+selectedLetter+"' ORDER BY  book_name");
        
          return "books";
    }

    /**
     * method is called to get books from database with sql inquiry searching by search string
     * @return list of found books
     * @return updated jsf page with results of sql inquiry
     * @throws DAOException
     */
    public String findBooksBySearch() throws DAOException {
             
             submitValues(' ', 1, -1, false);
        
          if (searchString.trim().length() == 0) {
            showBooks();
            return "books";
        }
        
       StringBuilder sql = new StringBuilder("SELECT book_id, book_name,exemplar_count, page_count,author_name,genre_name,image FROM book\n"+
                "INNER JOIN author a ON book.author_id = a.author_id\n" +
                "INNER JOIN genre g ON book.genre_id = g.genre_id");

        if(searchType == SearchType.AUTHOR){
            sql.append("where lower(author_name) like '%" +searchString + "%' order by book_name");
        }else if(searchType == SearchType.TITLE){
            sql.append("where lower(book_name) like '%" +searchString + "%' order by book_name");
        }
         getBooks(sql.toString());
         
           return "books";
    }

    /**
     * method is called when navigating to another page while viewing the search results in jsf page
     */
    public void selectPage() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        selectedPageNumber = Integer.valueOf(params.get("page_number"));
        requestFromPager = true;
        getBooks(currentSql);
    }

    /**
     * method is called to get pdf-file of book from the database
     * @param id  book id
     * @return pdf-file of the choosing book
     */
    public byte[] getContent(int id){
         ResultSet resultSet = null;
         byte[] content = null;
         Connection connection = ConnectionPool.getInstance().takeConnection();
         try(Statement statement = connection.createStatement()){
             resultSet = statement.executeQuery("select content from book where book_id=" + id);
             while(resultSet.next()){
                 content = resultSet.getBytes("content");
             }
         } catch (SQLException ex) {
           logger.log(Level.ERROR,"Can not upload book content");
        }finally{
             if(connection != null){
                 ConnectionPool.getInstance().returnConnection(connection);
             }
         }
         return content;
     }

    /**
     *  method is called to get book image of book from the database
     * @param id  book id
     * @return image of the choosing book
     */
    public byte[] getImage(int id){
          ResultSet resultSet = null;
         byte[] image = null;
         Connection connection = ConnectionPool.getInstance().takeConnection();
           try(Statement statement = connection.createStatement()){
               resultSet = statement.executeQuery("select image from book where book_id=" + id);
               while(resultSet.next()){
                 image = resultSet.getBytes("image");
             }
           } catch (SQLException ex) {
            logger.log(Level.ERROR,"Can not upload book image");
        }finally{
             if(connection != null){
                 ConnectionPool.getInstance().returnConnection(connection);
             }
         }
         return image;
     }

    /**
     * method is called to update search string
     * @param e new string which was input by user to find some books
     */
    public void searchStringChanged(ValueChangeEvent e)
    {
        searchString = e.getNewValue().toString();
    }

    /**
     * method is called to change type of search
     * @param e change type from Author to Title and backward
     */
    public void searchTypeChanged(ValueChangeEvent e)
    {
        searchType = (SearchType) e.getNewValue();
    }

    /**
     * method is called to get Russians letters to using search by letter
     * @return array of letters
     */
    public Character[] getRussianLetters() {
        Character[] letters = new Character[33];
        letters[0] = 'А';
        letters[1] = 'Б';
        letters[2] = 'В';
        letters[3] = 'Г';
        letters[4] = 'Д';
        letters[5] = 'Е';
        letters[6] = 'Ё';
        letters[7] = 'Ж';
        letters[8] = 'З';
        letters[9] = 'И';
        letters[10] = 'Й';
        letters[11] = 'К';
        letters[12] = 'Л';
        letters[13] = 'М';
        letters[14] = 'Н';
        letters[15] = 'О';
        letters[16] = 'П';
        letters[17] = 'Р';
        letters[18] = 'С';
        letters[19] = 'Т';
        letters[20] = 'У';
        letters[21] = 'Ф';
        letters[22] = 'Х';
        letters[23] = 'Ц';
        letters[24] = 'Ч';
        letters[25] = 'Ш';
        letters[26] = 'Щ';
        letters[27] = 'Ъ';
        letters[28] = 'Ы';
        letters[29] = 'Ь';
        letters[30] = 'Э';
        letters[31] = 'Ю';
        letters[32] = 'Я';

        return letters;
    }

    /**
     *   by the number of books found method  determines how many pages are needed to display books in page mode
     * @param totalBooksCount
     * @param booksCountOnPage
     */
    private void fillPageNumbers(long totalBooksCount, int booksCountOnPage) {
        int pageCount = booksCountOnPage > 0 ? (int) ((totalBooksCount / booksCountOnPage) + 1) : 0;
        pageNumbers.clear();
        for (int i = 1; i <= pageCount; i++) {
            pageNumbers.add(i);
        }

    }
        
            public ArrayList<Integer> getPageNumbers() {
        return pageNumbers;
    }

    public void setPageNumbers(ArrayList<Integer> pageNumbers) {
        this.pageNumbers = pageNumbers;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public SearchType getSearchType() {
        return searchType;
    }

    public void setSearchType(SearchType searchType) {
        this.searchType = searchType;
    }

    public Map<String, SearchType> getSearchList() {
        return searchList;
    }

    public ArrayList<Book> getCurrentBookList() {
        return currentBookList;
    }

    public void setTotalBooksCount(long booksCount) {
        this.totalBooksCount = booksCount;
    }

    public long getTotalBooksCount() {
        return totalBooksCount;
    }

    public int getSelectedGenreId() {
        return selectedGenreId;
    }

    public void setSelectedGenreId(int selectedGenreId) {
        this.selectedGenreId = selectedGenreId;
    }

    public char getSelectedLetter() {
        return selectedLetter;
    }

    public void setSelectedLetter(char selectedLetter) {
        this.selectedLetter = selectedLetter;
    }

    public int getBooksOnPage() {
        return booksOnPage;
    }

    public void setBooksOnPage(int booksOnPage) {
        this.booksOnPage = booksOnPage;
    }

    public void setSelectedPageNumber(long selectedPageNumber) {
        this.selectedPageNumber = selectedPageNumber;
    }

    public long getSelectedPageNumber() {
        return selectedPageNumber;
    }
         }
    

  
         
         
       
