package by.tarasevich.library.controllers;

import by.tarasevich.library.enumType.SearchType;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;


@ManagedBean
@RequestScoped
public class SearchTypeController {

    /**
     * @param searchList contains all types of searching book
     */
    private static Map<String, SearchType> searchList = new HashMap<String, SearchType>(); // хранит все виды поисков (по автору, по названию)

    /**
     *  constructor which is  initialize bundle of EN/RU support, cleared searchList if current locale was changed by the another language and than put search types back to the searchList
     */
    public SearchTypeController() {

        ResourceBundle bundle = ResourceBundle.getBundle("by.tarasevich.library.nls.messages", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        searchList.clear();
        searchList.put(bundle.getString("author_name"), SearchType.AUTHOR);
        searchList.put(bundle.getString("book_name"), SearchType.TITLE);
    }

    public Map<String, SearchType> getSearchList() {
        return searchList;
    }
}
