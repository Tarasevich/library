package by.tarasevich.library.controllers;

import java.io.Serializable;
import java.util.Locale;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


@ManagedBean
@SessionScoped
public class LocaleChanger implements Serializable {

    /**
     * current locale is Russian
     */
  private Locale currentLocale = new Locale("ru");

    public LocaleChanger() {
    }
/**
 * method changes locale from Ru to En 
 * and from En to Ru
 * @param localeCode 
 */
    public void changeLocale(String localeCode) {
        currentLocale = new Locale(localeCode);
    }

    /**
     * method is called to get current locale on page
     * @return current locale
     */
    public Locale getCurrentLocale() {
        return currentLocale;
    }



}
