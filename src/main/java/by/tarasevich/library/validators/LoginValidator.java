/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.tarasevich.library.validators;

import by.tarasevich.library.dao.DAOException;
import by.tarasevich.library.dao.impl.UserDAO;
import by.tarasevich.library.entity.User;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author рома
 */
@FacesValidator("by.tarasevich.library.validators.LoginValidator")
public class LoginValidator implements Validator{
    
    private static Logger logger = Logger.getLogger(LoginValidator.class);

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        ResourceBundle bundle = ResourceBundle.getBundle("by.tarasevich.library.nls.messages", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        UserDAO userDAO = new UserDAO();
  
    try{
        String newValue = value.toString();
       
      
        
        if (newValue.length() < 3) {
                throw new IllegalArgumentException(bundle.getString("login_length_error"));
            }
        if (!Character.isLetter(newValue.charAt(0))) {
                throw new IllegalArgumentException(bundle.getString("first_letter_error"));
            }
        for( User user : userDAO.showAllUsers()){
            if(user.getName().equalsIgnoreCase(newValue)){
                throw new IllegalArgumentException(bundle.getString("used_name")); 
            }
        }
            }catch(IllegalArgumentException e){
        FacesMessage message = new FacesMessage(e.getMessage());
        message.setSeverity(FacesMessage.SEVERITY_ERROR);
         userDAO.toErrorPage();
        throw new ValidatorException(message);
            }catch (DAOException ex) {
          logger.log(Level.ERROR,"Can not find user ib DB");
        }
    
    }
}
