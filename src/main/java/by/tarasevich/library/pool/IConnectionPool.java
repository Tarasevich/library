package by.tarasevich.library.pool;

import java.sql.Connection;

public interface IConnectionPool {
    Connection takeConnection();
    void returnConnection(Connection connection);
    void killPool() throws ConnectionPoolException;
}
