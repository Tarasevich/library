package by.tarasevich.library.pool.impl;

import by.tarasevich.library.pool.ConnectionPoolException;
import by.tarasevich.library.pool.IConnectionPool;
import com.mysql.jdbc.Driver;
import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class ConnectionPool implements IConnectionPool {

    private static final String JDBC_URL = "jdbc:mysql://localhost/library?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "12345";
    private static final String CONNECTION_AMOUNT = "5";
    private static final String DRIVER_LOCATION = "com.mysql.jdbc.Driver";
    private static final int DEFAULT_AMOUNT = 5;

    private static final Driver DRIVER;
    private static  Logger logger = Logger.getLogger(ConnectionPool.class);
    
    static {
        org.apache.log4j.PropertyConfigurator.configure("src/main/resources/by/tarasevich/logger/log4j.properties");
        try {
            DRIVER = new Driver();
        } catch (SQLException e){
            logger.log(Level.ERROR, "Can not find JDBC driver");
            throw new ExceptionInInitializerError(e);
        }
    }

    private static Lock lock = new ReentrantLock();
    private BlockingQueue<Connection> availableConnectionQueue;
    private BlockingQueue<Connection> usedConnectionQueue;

    private String url;
    private String user;
    private String password;
    private String locationOfDriver;
    private int connectionAmount;

    private static ConnectionPool instance;
    private static AtomicBoolean instanceCreated = new AtomicBoolean(false);

    public static ConnectionPool getInstance(){

        if(!instanceCreated.get()){
            lock.lock();
            try{
                if(instance == null){
                    instance = new ConnectionPool();
                    DriverManager.registerDriver(DRIVER);
                    instanceCreated.getAndSet(true);
                }
            } catch (SQLException e) {
                 logger.log(Level.ERROR,"Can not register JDBC driver");
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    private ConnectionPool(){
        try{
            this.url = JDBC_URL;
            this.user = USERNAME;
            this.password = PASSWORD;
            this.locationOfDriver = DRIVER_LOCATION;
            this.connectionAmount = Integer.parseInt(CONNECTION_AMOUNT);
            this.availableConnectionQueue = new ArrayBlockingQueue<>(connectionAmount);
            this.usedConnectionQueue = new ArrayBlockingQueue<>(connectionAmount);
            initPool();
        }catch (NumberFormatException e){
            this.connectionAmount = DEFAULT_AMOUNT;
        }catch (MissingResourceException e){
            logger.log(Level.ERROR,"JDBC! Not found resource bundle",e);
            throw new RuntimeException("JDBC! Not found resource bundle",e);
        }
    }


    @PostConstruct
    private void initPool() {
        logger.log(Level.INFO, "Creating pool");
        try {
            Class.forName(locationOfDriver);
            Locale.setDefault(Locale.ENGLISH);
            for (int i = 0; i < connectionAmount; i++) {
                Connection connection = DriverManager.getConnection(url, user, password);
                availableConnectionQueue.put(connection);
               logger.log(Level.INFO, "Connection" + i + " is created and put into the queue");
            }
        } catch (ClassNotFoundException e) {
           throw new RuntimeException("JDBC.Not fount driver");
        } catch (SQLException e) {
            logger.log(Level.ERROR, "SQLException occured!");
        } catch (InterruptedException e) {
           logger.log(Level.ERROR, "InterruptedException occured!");
        }
    }

    @Override
    public Connection takeConnection() {
        Connection connection = null;
        try{
            connection = availableConnectionQueue.take();
            usedConnectionQueue.put(connection);
        } catch (InterruptedException e) {
            logger.log(Level.ERROR,"Interrupted exception occurred while taking connection from the pool",e);
        }
       logger.log(Level.INFO,"Connection is taken");
        return connection;
    }

    @Override
    public void returnConnection(Connection connection) {
        try{
           logger.log(Level.INFO,"Trying to return connection to the connection pool..");
           availableConnectionQueue.put(connection);
           usedConnectionQueue.remove(connection);
           logger.log(Level.INFO,"Return connection to the connection pool..");
        } catch (InterruptedException e) {
            logger.log(Level.ERROR,"InterruptedException occurred");
        }
    }

    @Override
    public void killPool() throws ConnectionPoolException {
        logger.log(Level.INFO,"Destroying connection pool.");
        try{
            for(int i=0; i<connectionAmount; i++) {
                Connection connection = ConnectionPool.getInstance().takeConnection();
                connection.close();
               logger.log(Level.INFO,"Connection "+ i +" is destroyed.");
            }
        } catch (SQLException e){
            logger.log(Level.WARN,"Can't destroy connection pool.");
            throw new ConnectionPoolException("Can't destroy connection pool.", e);
        }
    }
}

