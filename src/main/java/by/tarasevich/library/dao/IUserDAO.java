package by.tarasevich.library.dao;

import by.tarasevich.library.entity.User;
import java.sql.SQLException;
import java.util.ArrayList;

public interface IUserDAO {

    void addUser(User user) throws DAOException;
    void deleteUser(User user) throws DAOException;
    void updateUser(User user) throws DAOException, SQLException;
    ArrayList<User> showAllUsers() throws DAOException;
    User getUserById(int id);
    boolean login(String user,String password,String role) throws SQLException;
    String signUpNewUser(User user) throws DAOException;


}
