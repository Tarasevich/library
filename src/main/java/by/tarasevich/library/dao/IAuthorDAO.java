package by.tarasevich.library.dao;

import by.tarasevich.library.entity.Author;
import by.tarasevich.library.entity.Book;
import java.util.ArrayList;

public interface IAuthorDAO {

    void deleteAuthor(int authorId) throws DAOException;
    ArrayList<Author> showAllAuthors() throws DAOException;
    ArrayList<Book> showAllAuthorsBooks(int authorId) throws DAOException;

}
