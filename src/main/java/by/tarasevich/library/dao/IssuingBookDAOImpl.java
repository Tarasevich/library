package by.tarasevich.library.dao;

import by.tarasevich.library.entity.IssuingBook;
import java.util.ArrayList;

public interface IssuingBookDAOImpl {

    void addNote(IssuingBook issuingBook) throws DAOException;
    void deleteNote(IssuingBook issuingBook) throws DAOException;
    void updateNote(IssuingBook issuingBook) throws DAOException;
    ArrayList<IssuingBook> showAllIsuuingBooks() throws DAOException;
}
