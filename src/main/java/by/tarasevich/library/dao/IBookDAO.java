package by.tarasevich.library.dao;


import by.tarasevich.library.entity.Book;
import java.util.ArrayList;

public interface IBookDAO {
    
    ArrayList<Book> getBooks(String sql) throws  DAOException;
    void addBook(Book book) throws DAOException;
    void deleteBook(Book book) throws DAOException;
    ArrayList<Book> showBooks() throws DAOException;
    ArrayList<Book> findBooksByGenre() throws DAOException;
    ArrayList<Book> findBooksByLetter() throws DAOException ;
    void findBooksBySearch()throws DAOException;
}
