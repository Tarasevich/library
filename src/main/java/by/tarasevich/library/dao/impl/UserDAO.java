package by.tarasevich.library.dao.impl;


import by.tarasevich.library.dao.DAOException;
import by.tarasevich.library.dao.IUserDAO;
import by.tarasevich.library.entity.User;
import by.tarasevich.library.pool.impl.ConnectionPool;
import java.sql.*;
import java.util.ArrayList;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

@ManagedBean(name = "userDAO")
@SessionScoped
public class UserDAO implements IUserDAO {


    private static UserDAO instance = new UserDAO();
    public static UserDAO getInstance(){
        return instance;
    }

    private static final String SQL_ADD_USER = "INSERT INTO user (user_id, number_ticket, user_name, password, role)" + "VALUES (?,?,?,?,?)";
    private static final String SQL_DELETE_USER = "DELETE FROM user WHERE user_id = ?";
    private static final String SQL_SHOW_ALL_USERS = "SELECT user_id, number_ticket,user_name, password, role  FROM user order by user_id";
    
    private  ArrayList<User> users = new ArrayList<>();
    private static Logger logger = Logger.getLogger(UserDAO.class);

    @Override
    public void addUser(User user) throws DAOException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_USER)) {
            preparedStatement.setInt(1,user.getId());
            preparedStatement.setInt(2,user.getTicketNumber());
            preparedStatement.setString(3,user.getName());
            preparedStatement.setString(4,user.getPassword());
            preparedStatement.setString(5,user.getRole());
            preparedStatement.executeUpdate();
            logger.log(Level.INFO,user.getName()+"was added from user list");
        } catch (SQLException e) {
             logger.log(Level.ERROR,"UserDAO error. Can not add new user!");
        }finally {
            if(connection != null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
    }

     @Override
    public void deleteUser(User user) throws DAOException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_USER)) {
            preparedStatement.setInt(1,user.getId());
            preparedStatement.executeUpdate();
            logger.log(Level.INFO,user.getName()+"was deleted from user list");
        } catch (SQLException e) {
            logger.log(Level.ERROR,"UserDAO error. Can not delete this user!");
        }finally {
            if(connection != null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
    }
    @Override
     public void updateUser(User user) throws DAOException, SQLException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement("update user set user_name = ? , number_ticket = ?, password = ? ,  role = ?  where user_id = ?")) {
             preparedStatement.setString(1,user.getName());
               preparedStatement.setInt(2,user.getTicketNumber());
             preparedStatement.setString(3,user.getPassword());
             preparedStatement.setString(5,user.getRole());
             preparedStatement.setInt(6,user.getId());
             preparedStatement.executeUpdate();
             logger.log(Level.INFO,user.getName()+"info was updated in the user list");
        } catch (SQLException e) {
            logger.log(Level.ERROR,"UserDAO error. Can update this user!");
        }finally {
            if(connection != null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
    }
     
  
    @Override
    public ArrayList<User> showAllUsers() throws DAOException {
       ResultSet resultSet = null;
       Connection connection = ConnectionPool.getInstance().takeConnection();
       try(Statement statement = connection.createStatement()){
           resultSet = statement.executeQuery(SQL_SHOW_ALL_USERS);
           while(resultSet.next()){
               User user = new User();
               user.setId(resultSet.getInt("user_id"));
               user.setName(resultSet.getString("user_name"));
               user.setTicketNumber(resultSet.getInt("number_ticket"));
               user.setPassword(resultSet.getString("password"));
               user.setRole(resultSet.getString("role"));
               users.add(user);
           }
           
       } catch (SQLException ex) {
           logger.log(Level.ERROR,"UserDAO error. Can not show all users!");
        }finally{
           if(connection != null){
               ConnectionPool.getInstance().returnConnection(connection);
           }
       }
      return users;
    }
    @Override
    public User getUserById(int id){
       ResultSet resultSet = null;
       Connection connection = ConnectionPool.getInstance().takeConnection();
        User user = new User();
       try(Statement statement = connection.createStatement()){
           resultSet = statement.executeQuery("select number_ticket,user_name,password, role where user_id = " + id);
           while(resultSet.next()){ 
               user.setId(resultSet.getInt("user_id"));
               user.setName(resultSet.getString("user_name"));
               user.setTicketNumber(resultSet.getInt("number_ticket"));
               user.setPassword(resultSet.getString("password"));
               user.setRole(resultSet.getString("role"));   
           }         
       } catch (SQLException ex) {
           logger.log(Level.ERROR,"UserDAO error. Can not show all users!");
        }finally{
           if(connection != null){
               ConnectionPool.getInstance().returnConnection(connection);
           }
       }
      return user;
    }
    

    
     public ArrayList<User> getUserList() throws DAOException {
       if(!users.isEmpty()){
           return users;
       }else{
           return showAllUsers();
       }
    }

     @Override
     public boolean login(String user,String password,String role) throws SQLException{
       ResultSet resultSet = null;
       Connection connection = ConnectionPool.getInstance().takeConnection();
       try(PreparedStatement statement = connection.prepareStatement("select user_name, password, role from user where user_name = ? and password = ? and role = ?")){
           statement.setString(1,user);
           statement.setString(2,password);
           statement.setString(3, role);
           resultSet = statement.executeQuery();
           if(resultSet.next()){
               return true;
           }else{
              FacesContext.getCurrentInstance().addMessage("null", new FacesMessage(FacesMessage.SEVERITY_WARN,"LoginDAO!","Wrong password message test!"));
	  return false; 
       }   
     }  catch (SQLException ex) {
           logger.log(Level.ERROR,"UserDAO error. Can not found login,password or role!");
            return false;
        }finally{
           if(connection != null){
               ConnectionPool.getInstance().returnConnection(connection);
           }
       }
    }

      public Integer howManyUsersRegistered() throws DAOException{
      return getUserList().size()+1;
}
      public Integer lastTicketNumber() throws DAOException{
          return getUserList().size()+99;
      }
      
   @Override
   public String signUpNewUser(User user) throws DAOException {
        Connection connection = ConnectionPool.getInstance().takeConnection(); 
        try(PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO user (user_id,user_name,password,role)" + "VALUES (?,?,?,?)")) {
            preparedStatement.setInt(1,user.getId());
            preparedStatement.setString(2,user.getName());
            preparedStatement.setString(3,user.getPassword());
            preparedStatement.setString(4,user.getRole());
            preparedStatement.executeUpdate(); 
            logger.log(Level.INFO,"New user "+user.getName()+ " was registered");
        } catch (SQLException e) {
             logger.log(Level.ERROR,"UserDAO error. Can not regix new user!");
        }finally {
            if(connection != null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
        return "exit";
    }  
    
  public String toRegistrPage(){
      return "registr";
  }
    public String toErrorPage(){
        return "error";
    }


}