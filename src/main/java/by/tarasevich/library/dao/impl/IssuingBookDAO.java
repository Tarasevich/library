package by.tarasevich.library.dao.impl;

import by.tarasevich.library.dao.DAOException;
import by.tarasevich.library.entity.Book;
import by.tarasevich.library.entity.IssuingBook;
import by.tarasevich.library.entity.User;
import by.tarasevich.library.pool.impl.ConnectionPool;
import java.sql.*;
import java.util.ArrayList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import by.tarasevich.library.dao.IssuingBookDAOImpl;

public class IssuingBookDAO implements IssuingBookDAOImpl {


    private static IssuingBookDAO instance = new IssuingBookDAO();
    public static IssuingBookDAO getInstance(){
        return instance;
    }
    
     private ArrayList<IssuingBook> bookList = new ArrayList<>();
     private static Logger logger = Logger.getLogger(IssuingBookDAO.class);

    private static final String SQL_SHOW_ISSUING_BOOKS = "SELECT  issuing_id, user_name, book_name, issuing_date, return_date, location FROM issuing_books order by issuing_id";
    private static final String SQL_ADD_RETURN_DATE = "SELECT * FROM issuing books UPDATE issuing books SET return_date = now() WHERE issuing_id = ?";
    private static final String SQL_ADD_NOTE = "insert into issuing_books (issuing_id,user_name,book_name,issuing_date,return_date,location) values(?,?,?,?,?,?)";
    private static final String SQL_DELETE_NOTE = "DELETE FROM issuing_books WHERE issuing_id = ?";
    private static final String SQL_UPDATE_NOTE = "UPDATE issuing books SET  return_date = ? , location = ?  WHERE issuing_id = ?";



    @Override
    public void addNote(IssuingBook issuingBook) throws DAOException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_NOTE)) {
            preparedStatement.setInt(1,issuingBook.getId());
             preparedStatement.setString(2,issuingBook.getUser());
              preparedStatement.setString(3,issuingBook.getBook());
            preparedStatement.setString(4, issuingBook.getIssuingDate());
            preparedStatement.setString(5, issuingBook.getReturnDate());
            preparedStatement.setString(6,issuingBook.getLocation());
            preparedStatement.executeUpdate();
             logger.log(Level.INFO,issuingBook.getUser()+" take "+ issuingBook.getBook() + " on the " + issuingBook.getIssuingDate());
        } catch (SQLException e) {
            logger.log(Level.ERROR,"Notes error. Can not add new note!");
        }finally {
            if(connection != null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
    }

    @Override
    public void deleteNote(IssuingBook issuingBook) throws DAOException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NOTE)) {
            preparedStatement.setInt(1,issuingBook.getId());
            preparedStatement.executeUpdate();
            logger.log(Level.INFO,issuingBook.getId() + " delete from the notes list");
        } catch (SQLException e) {
            logger.log(Level.ERROR,"Notes error. Can not delete note!");
        }finally {
            if(connection != null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
    }

    @Override
    public void updateNote(IssuingBook issuingBook) throws DAOException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_NOTE)) {
            preparedStatement.setString(1,issuingBook.getReturnDate());
            preparedStatement.setString(2,issuingBook.getLocation());
            preparedStatement.setInt(3,issuingBook.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.ERROR,"Notes error. Can not update this note!");
        }finally {
            if(connection != null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
    }

    @Override
    public ArrayList<IssuingBook> showAllIsuuingBooks() throws DAOException {
        ResultSet resultSet = null;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try(Statement statement = connection.createStatement()) {
            resultSet = statement.executeQuery(SQL_SHOW_ISSUING_BOOKS);
            while(resultSet.next()){
                IssuingBook book = new IssuingBook();
                book.setId(resultSet.getInt("issuing_id"));
                book.setUser(resultSet.getString("user_name"));
                book.setBook(resultSet.getString("book_name"));
                book.setIssuingDate(resultSet.getString("issuing_date"));
                book.setReturnDate(resultSet.getString("return_date"));
                book.setLocation(resultSet.getString("location"));
                bookList.add(book);
            }
        } catch (SQLException e) {
          logger.log(Level.ERROR,"Notes error. Can not show all notes!");
        }finally {
            if(connection != null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
        return bookList;
    }
    
     public ArrayList<User> getUsers() throws DAOException{
              ResultSet resultSet = null;
              ArrayList<User> users = new ArrayList<>();
              Connection connection = ConnectionPool.getInstance().takeConnection();
              try(Statement statement = connection.createStatement()){
              resultSet = statement.executeQuery("select user_name from user");
              while(resultSet.next()){
              User user = new User();
              user.setName(resultSet.getString("user_name"));
              users.add(user);
        }} catch (SQLException e) {
            logger.log(Level.ERROR,"Notes error. Can not show all users!");
        }finally {
            if(connection != null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
        return users;
              }
     
     public ArrayList<Book> getBooks() throws DAOException{
              ResultSet resultSet = null;
              ArrayList<Book> books = new ArrayList<>();
              Connection connection = ConnectionPool.getInstance().takeConnection();
              try(Statement statement = connection.createStatement()){
              resultSet = statement.executeQuery("select book_name from book");
              while(resultSet.next()){
              Book book = new Book();
              book.setName(resultSet.getString("book_name"));
              books.add(book);
        }} catch (SQLException e) {
           logger.log(Level.ERROR,"Notes error. Can not show all books!");
        }finally {
            if(connection != null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
        return books;
              }
     
        public ArrayList<IssuingBook> showNotes() throws DAOException {
        if(!bookList.isEmpty()){
            return bookList;
        }
     return showAllIsuuingBooks();
    }
}
