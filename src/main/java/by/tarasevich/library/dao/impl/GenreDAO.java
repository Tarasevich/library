package by.tarasevich.library.dao.impl;

import by.tarasevich.library.dao.DAOException;
import by.tarasevich.library.dao.IGenreDAO;
import by.tarasevich.library.entity.Book;
import by.tarasevich.library.entity.Genre;
import by.tarasevich.library.pool.impl.ConnectionPool;
import java.sql.*;
import java.util.ArrayList;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

@ManagedBean(eager = true)
@ApplicationScoped
public class GenreDAO implements IGenreDAO {


    private static GenreDAO instance = new GenreDAO();
    public static GenreDAO getInstance(){
        return instance;
    }

    private ArrayList<Genre> genreArrayList  = new ArrayList<>();
    private static Logger logger = Logger.getLogger(GenreDAO.class);

    private static final String SQL_DELETE_GENRE = "DELETE FROM genre WHERE genre_id = ?";
    private static final String SQL_SHOW_BOOK_GENRE = "SELECT book_name,genre_name FROM book INNER JOIN genre using(genre_id)";
    private static final String SQL_SHOW_ALL_GENRES = "SELECT genre_id,genre_name FROM genre";


    @Override
    public void deleteGenre(int genreId) throws DAOException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_GENRE)){
            preparedStatement.setInt(1,genreId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.log(Level.ERROR,"GenreDAO. Can not delete genre!");
        }finally {
            if(connection !=null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
    }

    @Override
    public Genre showBookGenre(Book book) throws DAOException {
        ResultSet resultSet = null;
        Genre genre = null;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try(Statement statement = connection.createStatement()) {
            resultSet = statement.executeQuery(SQL_SHOW_BOOK_GENRE);
            genre = (Genre) resultSet;
        } catch (SQLException e) {
           logger.log(Level.ERROR,"GenreDAO. Can not show book genre!");
        }finally {
            if(connection != null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
        return genre;
    }

    @Override
    public ArrayList<Genre> showAllGenres() throws DAOException {
        ResultSet resultSet = null;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try(Statement statement = connection.createStatement()) {
            resultSet = statement.executeQuery(SQL_SHOW_ALL_GENRES);
            while(resultSet.next()) {
                Genre genre = new Genre();
                  genre.setName(resultSet.getString("genre_name"));
                  genre.setId(resultSet.getInt("genre_id"));
                genreArrayList.add(genre);
            }
        } catch (SQLException e) {
           logger.log(Level.ERROR,"GenreDAO. Can not show genre list!");
        }finally {
            if(connection != null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
        return genreArrayList;
    }

    @Override
    public ArrayList<Genre> getGenreList() throws DAOException {
       if(!genreArrayList.isEmpty()){
           return genreArrayList;
       }else{
           return showAllGenres();
       }
    }
}
