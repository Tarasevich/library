package by.tarasevich.library.dao.impl;

import by.tarasevich.library.enumType.SearchType;
import by.tarasevich.library.dao.DAOException;
import by.tarasevich.library.dao.IBookDAO;
import by.tarasevich.library.entity.Author;
import by.tarasevich.library.entity.Book;
import by.tarasevich.library.pool.impl.ConnectionPool;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.faces.context.FacesContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;


public class BookDAO  implements IBookDAO {


    private static BookDAO instance = new BookDAO();

    public static BookDAO getInstance() {
        return instance;
    }

    private ArrayList<Book> bookList = new ArrayList<>();
    private SearchType searchType;// хранит выбранный тип поиска
    private String searchString; // хранит поисковую строку
    private static Map<String, SearchType> searchList = new HashMap<String, SearchType>(); // хранит все виды поисков (по автору, по названию)
    private static Logger logger = Logger.getLogger(BookDAO.class);

    private static final String SQL_ADD_BOOK = "INSERT INTO book ( book_id, author_id, book_name, genre_id, content, page_count, image, exemplar_count)" + "VALUES (?,?,?,?,?,?,?,?)";
    private static final String SQL_DELETE_BOOK = "DELETE FROM book WHERE book_id = ?";
    public static final String SQL_SHOW_ALL_BOOKS = "select book_id,book_name, author_name, genre_name, content, image, page_count,exemplar_count FROM book\n" +
            "inner join author a on a.author_id = book.author_id \n" +
            "inner join genre g on  g.genre_id  =  book.genre_id \n" +
            "order by book_id";


    @Override
    public ArrayList<Book> getBooks(String sql) throws DAOException {
        ResultSet resultSet = null;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try (Statement statement = connection.createStatement()) {
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Book book = new Book();
                book.setId(resultSet.getInt("book_id"));
                book.setName(resultSet.getString("book_name"));
                book.setGenre(resultSet.getString("genre_name"));
                book.setAuthor(resultSet.getString("author_name"));
                book.setExemplarCount(resultSet.getInt("exemplar_count"));
                book.setPageCount(resultSet.getInt("page_count"));
                book.setImage(resultSet.getBytes("image"));
                bookList.add(book);
            }
        } catch (SQLException e) {
            logger.log(Level.ERROR, "BookDAO.Error with getting book list");
        } finally {
            if (connection != null) {
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
        return bookList;
    }


    @Override
    public void addBook(Book book) throws DAOException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_BOOK)) {
            preparedStatement.setInt(1, book.getId());
            preparedStatement.setString(2, book.getAuthor());
            preparedStatement.setString(3, book.getName());
            preparedStatement.setString(4, book.getGenre());
            preparedStatement.setBytes(5, book.getContent());
            preparedStatement.setInt(6, book.getPageCount());
            preparedStatement.setBinaryStream(7, book.getFileImage().getInputstream());
            preparedStatement.setInt(8, book.getExemplarCount());
            preparedStatement.executeUpdate();
            logger.log(Level.INFO,"Book " +book.getName()+ " was added to the book list");
        } catch (SQLException e) {
            logger.log(Level.ERROR, "Error with insertion a book");
        } catch (IOException ex) {
            logger.log(Level.ERROR, "Error with book image");
        } finally {
            if (connection != null) {
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
    }

    @Override
    public void deleteBook(Book book) throws DAOException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_BOOK)) {
            preparedStatement.setInt(1, book.getId());
            preparedStatement.executeUpdate();
            logger.log(Level.INFO,"Book " +book.getName()+ " was deleted from the book list");
        } catch (SQLException e) {
            logger.log(Level.ERROR, "BookDAO. Can not delete book");
        } finally {
            if (connection != null) {
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
    }


    @Override
    public ArrayList<Book> showBooks() throws DAOException {
        if (!bookList.isEmpty()) {
            return bookList;
        }
        return getBooks(SQL_SHOW_ALL_BOOKS);
    }

    @Override
    public ArrayList<Book> findBooksByGenre() throws DAOException {

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        Integer genre_id = Integer.valueOf(params.get("genre_id"));

        return getBooks("SELECT book_id,book_name,page_count, exemplar_count, author_name, genre_name, image FROM book\n" +
                "INNER JOIN author a ON book.author_id = a.author_id\n" +
                "INNER JOIN genre g ON book.genre_id = g.genre_id\n" +
                "WHERE g.genre_id =" + genre_id + "  ORDER BY  book_name");
    }


    @Override
    public ArrayList<Book> findBooksByLetter() throws DAOException {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String searchLetter = params.get("letter");

        return getBooks("SELECT book_id, book_name,exemplar_count, page_count,author_name,genre_name,image FROM book\n" +
                "INNER JOIN author a ON book.author_id = a.author_id\n" +
                "INNER JOIN genre g ON book.genre_id = g.genre_id\n" +
                "WHERE substr(book_name,1,1) = '" + searchLetter + "' ORDER BY  book_name");
    }

    @Override
    public void findBooksBySearch() throws DAOException {

        if (searchString.trim().length() == 0) {
            showBooks();
            return;
        }

        StringBuilder sql = new StringBuilder("SELECT book_id, book_name,exemplar_count, page_count,author_name,genre_name,image FROM book\n" +
                "INNER JOIN author a ON book.author_id = a.author_id\n" +
                "INNER JOIN genre g ON book.genre_id = g.genre_id");

        if (searchType == SearchType.AUTHOR) {
            sql.append("where lower(author_name) like '%" + searchString + "%' order by book_name");
        } else if (searchType == SearchType.TITLE) {
            sql.append("where lower(book_name) like '%" + searchString + "%' order by book_name");
        }
        getBooks(sql.toString());
    }

    public ArrayList<Author> getAuthors() throws DAOException {
        ResultSet resultSet = null;
        ArrayList<Author> authors = new ArrayList<>();
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try (Statement statement = connection.createStatement()) {
            resultSet = statement.executeQuery("select author_name from author");
            while (resultSet.next()) {
                Author author = new Author();
                author.setName(resultSet.getString("author_name"));
                authors.add(author);
            }
        } catch (SQLException e) {
            logger.log(Level.ERROR, "BookDAO.Can not show all authors!");
        } finally {
            if (connection != null) {
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
        return authors;
    }


    public byte[] getContent(int id) {
        ResultSet resultSet = null;
        byte[] content = null;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try (Statement statement = connection.createStatement()) {
            resultSet = statement.executeQuery("select content from book where book_id=" + id);
            while (resultSet.next()) {
                content = resultSet.getBytes("content");
            }
          logger.log(Level.INFO,"Book "+id+" was opened by user");
        } catch (SQLException ex) {
            logger.log(Level.ERROR, "BookDAO. Can not get book content!");
        } finally {
            if (connection != null) {
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
        return content;
    }

    public byte[] getImage(int id) {
        ResultSet resultSet = null;
        byte[] image = null;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try (Statement statement = connection.createStatement()) {
            resultSet = statement.executeQuery("select image from book where book_id=" + id);
            while (resultSet.next()) {
                image = resultSet.getBytes("image");
            }
        } catch (SQLException ex) {
            logger.log(Level.ERROR, "BookDAO.Can not get book image!");
        } finally {
            if (connection != null) {
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
        return image;
    }


    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public SearchType getSearchType() {
        return searchType;
    }

    public void setSearchType(SearchType searchType) {
        this.searchType = searchType;
    }

    public Map<String, SearchType> getSearchList() {
        return searchList;

    }
}

    

  



    

  


 

