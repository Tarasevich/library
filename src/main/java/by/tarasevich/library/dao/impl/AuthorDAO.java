package by.tarasevich.library.dao.impl;


import by.tarasevich.library.dao.DAOException;
import by.tarasevich.library.dao.IAuthorDAO;
import by.tarasevich.library.entity.Author;
import by.tarasevich.library.entity.Book;
import by.tarasevich.library.pool.impl.ConnectionPool;
import java.sql.*;
import java.util.ArrayList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class AuthorDAO implements IAuthorDAO {

    /**
     * creating singleton of this class
     */
    private static AuthorDAO instance = new AuthorDAO();
    public static AuthorDAO getInstance(){
        return instance;
    }

    /** @param authorList contains list of all authors getting from database
     *  @param logger  logger for this class
     */
    public  ArrayList<Author> authorList = new ArrayList<>();
    private static Logger logger = Logger.getLogger(AuthorDAO.class);

    /**
     * @param SQL_DELETE_AUTHOR sql inquiry to delete choosing author from database
     * @param SQL_SHOW_ALL_AUTHORS sql inquiry to get all authors from database
     * @param SQL_SHOW_ALL_AUTHORS_BOOKS sql inquiry to show all books which was published by choosing author from database
     */
    private static final String SQL_DELETE_AUTHOR = "DELETE FROM author WHERE author_id = ?";
    private static final String SQL_SHOW_ALL_AUTHORS = "SELECT author_name FROM author";
    private static final String SQL_SHOW_ALL_AUTHORS_BOOKS = "SELECT author_id,author_name, book_name FROM author INNER JOIN book using(author_id) WHERE author_id = ?";


    /**
     *  method is called to delete choosing author from database
     * @param authorId choosing author
     * @throws DAOException
     */
    @Override
    public void deleteAuthor(int authorId) throws DAOException {
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_AUTHOR)) {
            preparedStatement.setInt(1,authorId);
            preparedStatement.executeUpdate();
            logger.log(Level.INFO,"Аuthor: "+authorId +" was deleted from database");
        } catch (SQLException e) {
            logger.log(Level.ERROR,"AuthorDAO. Can not delete author!");
        }finally {
            if(connection != null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }

    }

    /**
     * method is called to get all authors from database
     * @return list of authors
     * @throws DAOException
     */
    @Override
    public ArrayList<Author> showAllAuthors() throws DAOException {
        ResultSet resultSet = null;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try(Statement statement = connection.createStatement()) {
            resultSet = statement.executeQuery(SQL_SHOW_ALL_AUTHORS);
            while(resultSet.next()){
                Author author = new Author();
                author.setName(resultSet.getString("author_name"));
                authorList.add(author);
            }
        } catch (SQLException e) {
            logger.log(Level.ERROR,"AuthorDAO. Can not show all authors!");
        }finally {
            if(connection != null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
        return  authorList;
    }

    /**
     * method is called to get all books which was published by choosing author from database
     * @param authorId choosing author
     * @return list of books
     * @throws DAOException
     */
    @Override
    public ArrayList<Book> showAllAuthorsBooks(int authorId) throws DAOException {
        ResultSet resultSet = null;
        ArrayList<Book> books = null;
        Connection connection = ConnectionPool.getInstance().takeConnection();
        try(Statement statement = connection.createStatement()) {
            resultSet = statement.executeQuery(SQL_SHOW_ALL_AUTHORS_BOOKS);
            books = (ArrayList<Book>) resultSet;
        } catch (SQLException e) {
           logger.log(Level.ERROR,"AuthorDAO. Can not show all authors books!");
        }finally {
            if(connection != null){
                ConnectionPool.getInstance().returnConnection(connection);
            }
        }
        return books;
    }


}
