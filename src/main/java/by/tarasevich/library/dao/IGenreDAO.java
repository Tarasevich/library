package by.tarasevich.library.dao;

import by.tarasevich.library.entity.Book;
import by.tarasevich.library.entity.Genre;
import java.util.ArrayList;

public interface IGenreDAO {

    void deleteGenre(int genreId) throws DAOException;
    Genre showBookGenre(Book book) throws DAOException;
    ArrayList<Genre> showAllGenres() throws DAOException;
     ArrayList<Genre> getGenreList() throws DAOException;
}
