CREATE TABLE `author` (
  `author_id` smallint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT 'учетный номер автора',
  `author_name` varchar(40) NOT NULL COMMENT 'имя автора',
  PRIMARY KEY (`author_id`),
  UNIQUE KEY `id_UNIQUE` (`author_id`),
  UNIQUE KEY `ФИО_UNIQUE` (`author_name`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='Авторы книг';


CREATE TABLE `book` (
  `book_id` mediumint(4) unsigned NOT NULL AUTO_INCREMENT COMMENT 'учетный номер книги',
  `book_name` varchar(50) NOT NULL COMMENT 'название книги',
  `genre_id` tinyint(2) unsigned NOT NULL COMMENT 'жанр книги',
  `author_id` smallint(3) unsigned NOT NULL COMMENT 'автор книги',
  `page_count` smallint(3) unsigned NOT NULL COMMENT 'количество страниц ',
  `content` longblob COMMENT 'электронный формат книги',
  `image` longblob COMMENT 'Изображение обложки книги',
  `exemplar_count` smallint(2) unsigned NOT NULL COMMENT 'количество экземпляров книги',
  PRIMARY KEY (`book_id`),
  UNIQUE KEY `id_UNIQUE` (`book_id`),
  UNIQUE KEY `name_UNIQUE` (`book_name`),
  KEY `fk_author_idx` (`author_id`),
  KEY `fk_genre_idx` (`genre_id`),
  CONSTRAINT `author_ID` FOREIGN KEY (`author_id`) REFERENCES `author` (`author_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `genreID` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`genre_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COMMENT='Каталог книг';

CREATE TABLE `genre` (
  `genre_id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT COMMENT 'учетный номер жанра',
  `genre_name` varchar(40) NOT NULL COMMENT 'жанр книги',
  PRIMARY KEY (`genre_id`),
  UNIQUE KEY `id_UNIQUE` (`genre_id`),
  UNIQUE KEY `name_UNIQUE` (`genre_name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE `issuing_books` (
  `issuing_id` mediumint(4) unsigned NOT NULL AUTO_INCREMENT COMMENT 'порядковый номер записи',
  `user_name` varchar(50) NOT NULL,
  `book_name` varchar(50) NOT NULL COMMENT 'учетный номер книги',
  `issuing_date` varchar(10) DEFAULT NULL COMMENT 'дата выдачи',
  `return_date` varchar(10) DEFAULT NULL COMMENT 'дата возврата',
  `location` enum('Дом','Зал') NOT NULL,
  PRIMARY KEY (`issuing_id`),
  UNIQUE KEY `id_UNIQUE` (`issuing_id`),
  KEY `bookID_idx` (`book_name`),
  KEY `usFK_idx` (`user_name`),
  CONSTRAINT `bookFK` FOREIGN KEY (`book_name`) REFERENCES `book` (`book_name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `userFK` FOREIGN KEY (`user_name`) REFERENCES `user` (`user_name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='учет выдачи книг';

CREATE TABLE `user` (
  `user_id` mediumint(4) unsigned NOT NULL AUTO_INCREMENT COMMENT 'учетный номер читателя',
  `number_ticket` mediumint(4) unsigned DEFAULT NULL COMMENT 'номер читательского абонемента',
  `user_name` varchar(40) NOT NULL COMMENT 'имя читателя',
  `password` varchar(15) NOT NULL,
  `role` enum('admin','reader','librarian') NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `id_UNIQUE` (`user_id`),
  UNIQUE KEY `name_UNIQUE` (`user_name`),
  UNIQUE KEY `№ ticket_UNIQUE` (`number_ticket`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Пользователи';





