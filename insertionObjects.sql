-- author
INSERT INTO `author` (`id`,`name`) VALUES (1, 'Дэн Браун');
INSERT INTO `author` (`id`,`name`) VALUES (2, 'Федор Достоевский');
INSERT INTO `author` (`id`,`name`) VALUES (3, 'Эрик Рис');
INSERT INTO `author` (`id`,`name`) VALUES (4, 'Данте Алигьери');
INSERT INTO `author` (`id`,`name`) VALUES (5, 'Уильям Шекспир');
INSERT INTO `author` (`id`,`name`) VALUES (6, 'Лев Толстой');
INSERT INTO `author` (`id`,`name`) VALUES (7, 'Жюль Верн');
INSERT INTO `author` (`id`,`name`) VALUES (8, 'Томас Харрис');
INSERT INTO `author` (`id`,`name`) VALUES (9, 'Джоан Роулинг');
INSERT INTO `author` (`id`,`name`) VALUES (10, 'Джованни Бокаччо');
INSERT INTO `author` (`id`,`name`) VALUES (11, 'Андреа Пирло');
INSERT INTO `author` (`id`,`name`) VALUES (12, 'Эшли Вэнс');
INSERT INTO `author` (`id`,`name`) VALUES (13, 'Ха Джун Чанг');
INSERT INTO `author` (`id`,`name`) VALUES (14, 'Кэмерон Диас');
INSERT INTO `author` (`id`,`name`) VALUES (15, 'Джон Фаулз');
INSERT INTO `author` (`id`,`name`) VALUES (16, 'Марио Пьюзо');
INSERT INTO `author` (`id`,`name`) VALUES (17, 'Виктор Гюго');
INSERT INTO `author` (`id`,`name`) VALUES (18, 'Борис Акунин');
INSERT INTO `author` (`id`,`name`) VALUES (19, 'Габриэль Гарсия Маркес');
INSERT INTO `author` (`id`,`name`) VALUES (20, 'Агата Кристи');
INSERT INTO `author` (`id`,`name`) VALUES (21, 'Маргарэт Митчелл');

-- book
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (1,'Ангелы и демоны', null, 108, 6, 1, null, 1);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (2,'Бесы', null, 73, 3, 2, null, 20);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (3,'Бизнес с нуля. Метод LEAN STARTUP', null, 27, 5, 3, null, 20);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (4,'Божественная комедия. Новая жизнь', null, 86, 1, 4, null, 20);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (5,'Веницианский купец', null, 97, 2, 5, null, 17);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (6,'Вечный муж', null, 75, 3, 2, null, 20);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (7,'Война и мир', null, 155, 3, 6, null, 40);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (8,'Вокруг света за 80 дней', null, 134, 1, 7, null, 15);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (9,'Ганнибал', null, 92, 6, 8, null, 33);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (10,'Гарри Поттер и философский камень', null, 178, 6, 9, null, 41);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (11,'Гарри Поттер и тайная комната', null, 191, 6, 9, null, 33);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (12,'Гарри Поттер и узник азкабана', null, 164, 6, 9, null, 26);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (13,'Гарри Поттер и кубок огня', null, 155, 6, 9, null, 22);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (14,'Декамерон', null, 134, 2, 10, null, 8);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (15,'Дети капитана Гранта', null, 88, 1, 7, null, 13);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (16,'Думаю - следовательно, играю!', null, 141, 4, 11, null, 4);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (17,'Дэниэл Мартин', null, 99, 6, 15, null, 11);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (18,'Илон Маск. Tesla, SpaceX и дорога в будущее', null, 103, 4, 12, null, 5);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (19,'Как устроена экономика', null, 102, 5, 13, null, 25);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (20,'Книга о теле', null, 77, 5, 14, null, 20);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (21,'Код Да Винчи', null, 112, 6, 1, null, 31);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (22,'Коллекционер', null, 96, 6, 15, null, 11);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (23,'Красный дракон', null, 108, 6, 8, null, 18);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (24,'Крестный отец', null, 160, 1, 16, null, 8);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (25,'Кроткая', null, 100, 3, 2, null, 20);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (26,'Любовница французского лейтенанта', null, 104, 6, 15, null, 9);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (27,'Молчание ягнят', null, 171, 1, 8, null, 21);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (28,'Отверженные', null, 108, 2, 17, null, 11);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (29,'Подросток', null, 111, 3, 2, null, 19);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (30,'Ричард II', null, 82, 2, 5, null, 20);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (31,'Ричард III', null, 108, 2, 5, null, 7);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (32,'Слабое сердце', null, 88, 3, 2, null, 31);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (33,'Сон смешного человека', null, 74, 3, 2, null, 20);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (34,'Статский советник', null, 189, 3, 18, null, 20);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (35,'Сто лет одиночества', null, 201, 1, 19, null, 37);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (36,'Точка обмана', null, 77, 6, 1, null, 5);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (37,'Турецкий гамбит', null, 118, 3, 18, null, 20);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (38,'Убийство в восточном экспрессе', null, 154, 1, 20, null, 28);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (39,'Унесенные ветром', null, 108, 1, 21, null, 17);
INSERT INTO `book` (`id`,`name`, `content`, `page_count`, `genre_id`, `author_id`, `image`, `exemplar_count`) VALUES (40,'Фьяметта', null, 79, 2, 10, null, 20);

-- genre

INSERT INTO `genre` (`id`,`name`) VALUES (1,'Зарубежная классика');
INSERT INTO `genre` (`id`,`name`) VALUES (2,'Европейская литература');
INSERT INTO `genre` (`id`,`name`) VALUES (3,'Русская классика');
INSERT INTO `genre` (`id`,`name`) VALUES (4,'Биография');
INSERT INTO `genre` (`id`,`name`) VALUES (5,'Образование');
INSERT INTO `genre` (`id`,`name`) VALUES (6,'Зарубежная литература');


-- user

INSERT INTO `user` (`id`,`number_ ticket`,`name`,`password`,`role`,`count_books`) VALUES (1,null, 'admin','admin', 'admin',null);
INSERT INTO `user` (`id`,`number_ ticket`,`name`,`password`,`role`,`count_books`) VALUES (2, 1, 'Арни Шварцнеггер', '1234', 'reader', 4);
INSERT INTO `user` (`id`,`number_ ticket`,`name`,`password`,`role`,`count_books`) VALUES (3, 2, 'Том Круз', 'qwerty', 'reader', 12);
INSERT INTO `user` (`id`,`number_ ticket`,`name`,`password`,`role`,`count_books`) VALUES (4, 3, 'Сильвестр Сталлоне', '4321', 'reader', 3);
INSERT INTO `user` (`id`,`number_ ticket`,`name`,`password`,`role`,`count_books`) VALUES (5, 4, 'Иван Кузнецов', '4321', 'reader', 15);
INSERT INTO `user` (`id`,`number_ ticket`,`name`,`password`,`role`,`count_books`) VALUES (6, 5, 'Дмитрий Медведев', 'zxcv', 'reader', 14);
INSERT INTO `user` (`id`,`number_ ticket`,`name`,`password`,`role`,`count_books`) VALUES (7, 6, 'Константин Эрнст', 'rewq', 'reader', 7);
INSERT INTO `user` (`id`,`number_ ticket`,`name`,`password`,`role`,`count_books`) VALUES (8, 7, 'Джордж Клуни', '0987', 'reader', 11);
INSERT INTO `user` (`id`,`number_ ticket`,`name`,`password`,`role`,`count_books`) VALUES (9, 8, 'Брэдд Питт', '00066', 'reader', 22);
INSERT INTO `user` (`id`,`number_ ticket`,`name`,`password`,`role`,`count_books`) VALUES (10,9, 'Оуэн Уилсон', '121212', 'reader', 1);

-- issuing books

INSERT INTO `issuing books` (`issuing_id`,`librarian_id`, `user_id`, `book_id`, `issuing_date`, `return_date`, `location`) VALUES (1, 1, 1, 15, '2017-12-01 17:31:12', null, 'Дом');
INSERT INTO `issuing books` (`issuing_id`,`librarian_id`, `user_id`, `book_id`, `issuing_date`, `return_date`, `location`) VALUES (2, 1, 3, 10, '2017-12-03 12:02:44', null, 'Дом');
INSERT INTO `issuing books` (`issuing_id`,`librarian_id`, `user_id`, `book_id`, `issuing_date`, `return_date`, `location`) VALUES (3, 2, 1, 6,  '2017-12-06 14:35:06', '2017-12-07', 'Дом');
INSERT INTO `issuing books` (`issuing_id`,`librarian_id`, `user_id`, `book_id`, `issuing_date`, `return_date`, `location`) VALUES (4, 3, 2, 37, '2017-12-06 11:30:02', '2017-12-06', 'Зал');
INSERT INTO `issuing books` (`issuing_id`,`librarian_id`, `user_id`, `book_id`, `issuing_date`, `return_date`, `location`) VALUES (5, 2, 3, 31, '2017-12-02 11:51:17',  null, 'Зал');
INSERT INTO `issuing books` (`issuing_id`,`librarian_id`, `user_id`, `book_id`, `issuing_date`, `return_date`, `location`) VALUES (6, 2, 1, 12, '2017-12-04 11:33:19', '2017-12-04', 'Зал');
INSERT INTO `issuing books` (`issuing_id`,`librarian_id`, `user_id`, `book_id`, `issuing_date`, `return_date`, `location`) VALUES (7, 1, 2, 19, '2017-12-06 19:32:45', '2017-12-08', 'Дом');
